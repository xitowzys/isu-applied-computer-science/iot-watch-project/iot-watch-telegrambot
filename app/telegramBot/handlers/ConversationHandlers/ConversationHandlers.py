from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler

from app.telegramBot.utils import HandlersContainer

handlerContainer = HandlersContainer()

convMainHandler = ConversationHandler(
    entry_points=[
        CommandHandler(
            "start", handlerContainer["mainHandler"]["mainHandler"])],

    states={
        # "NAME_PRODUCT": [convTolokoSettingsHandler],
        "SHOW_MAIN_KEYBOARD": [MessageHandler(
            filters=Filters.text, callback=handlerContainer["mainHandler"]["messageHandler"])],
    },

    fallbacks=[],
    allow_reentry=True
)