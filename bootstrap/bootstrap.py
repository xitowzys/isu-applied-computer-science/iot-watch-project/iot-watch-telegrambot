import sys
from loguru import logger as log
from app.telegramBot.utils import HandlersContainer
from . import start_bot

def logger_configuration() -> None:

    # TODO: Добавить создание папки logs если её нету

    # logger.add("./logs/logs.log", format="({time}) {level} {message}",
    #            level="DEBUG", rotation="10 KB", compression="zip", serialize=True)

    log.remove()

    log.add(
        sys.stdout, colorize=True, format="(<level>{level}</level>) [<green>{time:HH:mm:ss}</green>] ➤ <level>{message}</level>")


def bootstrap() -> None:
    """Launching the application
    """
    logger_configuration()

    handlerContainer = HandlersContainer("app/telegramBot/handlers")

    start_bot()