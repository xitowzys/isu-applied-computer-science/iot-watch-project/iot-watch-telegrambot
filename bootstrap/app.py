import telegram
from telegram.ext import Updater
from loguru import logger as log
from config import TELEGRAM_BOT_TOKEN


def start_bot() -> None:
    from app.telegramBot.handlers.ConversationHandlers.ConversationHandlers import convMainHandler

    try:

        updater = Updater(TELEGRAM_BOT_TOKEN)

        dispatcher = updater.dispatcher

        dispatcher.add_handler(convMainHandler)

        updater.start_polling()
        log.success("Бот активирован")

        updater.idle()
    except telegram.error.Unauthorized as e:
        log.error(f"Ошибка активации бота\n{e}")
        exit(1)
